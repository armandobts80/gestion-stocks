﻿using Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeederDB
{
    class Program
    {
        static void Main(string[] args)
        {
            string conString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\Users\\Armando\\Desktop\\GestionStocks\\GestionStock\\bin\\Debug\\Gestion_Stock.accdb;Persist Security Info=True;Jet OLEDB:Database Password=stock@bts**";
            
            
             DAL dal = new DAL(conString);
            int funcoesApagadas = dal.ApagarTodasFuncoes();
            Console.WriteLine($"Apagou {funcoesApagadas} funcoes.");

            Funcao funcao1 = new Funcao("CDC");
            Funcao funcao2 = new Funcao("REQ");
            Funcao funcao3 = new Funcao("RPC");
            Funcao funcao4 = new Funcao("AA1");
            Funcao funcao5 = new Funcao("BB2");
            Funcao funcao6 = new Funcao("CC3");
            Funcao funcao7 = new Funcao("DD4");
            Funcao funcao8 = new Funcao("EE5");
            Funcao funcao9 = new Funcao("FF6");
            Funcao funcao10 = new Funcao("GG7");
            Funcao funcao11 = new Funcao("HH8");
            Funcao funcao12 = new Funcao("II9");
            
            dal.InsertFuncao(funcao1);
            dal.InsertFuncao(funcao2);
            dal.InsertFuncao(funcao3);
            dal.InsertFuncao(funcao4);
            dal.InsertFuncao(funcao5);
            dal.InsertFuncao(funcao6);
            dal.InsertFuncao(funcao7);
            dal.InsertFuncao(funcao8);
            dal.InsertFuncao(funcao9);
            dal.InsertFuncao(funcao10);
            dal.InsertFuncao(funcao11);
            dal.InsertFuncao(funcao12);

            Console.WriteLine("Terminou");
            Console.ReadLine();

        }
    }
}
