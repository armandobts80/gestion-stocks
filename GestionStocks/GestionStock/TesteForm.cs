﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;

namespace GestionStock
{
    public partial class TesteForm : Form
    {
        public TesteForm()
        {
            InitializeComponent();
            
            textBoxMatricula.Visible = false;
            textBoxLogin.Visible = false;
            textBoxNome.Visible = false;

            botaoConfirmaAdicionar.Visible = false;

            labelAdicionar.Visible = false;

            labelMatricula.Visible = false;
            labelLogin.Visible = false;
            labelNome.Visible = false;
            botaoApagarOK.Visible = false;
        }

        private void botao1_Click(object sender, EventArgs e)
        {
            string ligacaoString = Properties.Settings.Default.Gestion_Stock_ConnectionString;
            OleDbConnection ligacao = new OleDbConnection(ligacaoString);

            ligacao.Open();

            string comandoString = "select * from Colaborador;";
            OleDbCommand comando = new OleDbCommand(comandoString, ligacao);

            OleDbDataReader leitorDados = comando.ExecuteReader();

            DataTable tabelaDados = new DataTable();

            tabelaDados.Load(leitorDados);



            dataGridView1.DataSource = tabelaDados;

            ligacao.Close();
        }

        private void carregar2_Click(object sender, EventArgs e)
        {
            //dataGridView1 = new DataGridView();

            var listaColaboradores = new List<Colaborador>();
            DAL dal = new DAL();
            listaColaboradores = dal.SelectColaboradores();

            //var list = new BindingList<Colaborador>(listaColaboradores);

            DataTable dataTable = new DataTable();

            dataTable = dal.SelectDataTableColaboradores();




            dataGridView1.DataSource = dataTable;
            dataGridView1.Refresh();


        }

        private void botaoConfirmaAdicionar_Click(object sender, EventArgs e)
        {
            bool insereOK = true;
            bool inseriuOK = false;
            string textoErro = "";
            if (string.IsNullOrWhiteSpace(textBoxMatricula.Text)) { insereOK = false; textoErro += "\nErro: Preencher Matricula!";  }
            if (string.IsNullOrWhiteSpace(textBoxLogin.Text)) { insereOK = false; textoErro += "\nErro: Preencher Login!"; }
            if (string.IsNullOrWhiteSpace(textBoxNome.Text)) { insereOK = false; textoErro += "\nErro: Preencher Nome!"; }

            if (insereOK)
            {
                if (!BLL.ValidarNovaMatricula(uint.Parse(textBoxMatricula.Text))) 
                { 
                    insereOK = false;
                    textoErro += "\nMatricula ja existe.";
                }
            }

            if (!insereOK) { labelAdicionar.Text = textoErro; }

            if (insereOK) 
            {
               inseriuOK = new DAL().InserirColaborador(uint.Parse(textBoxMatricula.Text),textBoxLogin.Text,textBoxNome.Text,"ccc");
            
            }

            if (inseriuOK)
            {


                labelAdicionar.Text = "inseriu novo colaborador ok";

            }

        }    
            
          
            private void label2_Click(object sender, EventArgs e)
            {

            }



            private void botaoAdicionar_Click(object sender, EventArgs e)
            {
            textBoxMatricula.Visible = true;
            textBoxLogin.Visible = true;
            textBoxNome.Visible = true;

            botaoConfirmaAdicionar.Visible = true;

            labelAdicionar.Visible = true;

            labelMatricula.Visible = true;
            labelLogin.Visible = true;
            labelNome.Visible = true;

        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
       {
            textBoxMatricula.Visible = true;
            textBoxLogin.Visible = true;
            textBoxNome.Visible = true;

            labelMatricula.Visible = true;
            labelLogin.Visible = true;
            labelNome.Visible = true;

            string matricula = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            string login = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            string nome = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();

            textBoxMatricula.Text = matricula;
            textBoxLogin.Text = login;
            textBoxNome.Text = nome;

        }

        private void botaoApagar_Click(object sender, EventArgs e)
        {

            botaoApagarOK.Visible = true;

        }

        private void botaoApagarOK_Click(object sender, EventArgs e)
        {
            bool apagarOK = false;
            bool apagouOK= false;
            bool existeOK = false;

            DAL dal = new DAL();

            uint matricula = uint.Parse(textBoxMatricula.Text);

            string textoLabel = "";

            Colaborador colaboradorApagar = dal.SelectColaboradorSingle(matricula);

            if (colaboradorApagar != null) existeOK = true;

            if (existeOK)
                apagarOK = BLL.ValidarApagarColaborador(matricula);
            else
                textoLabel = "colaborador nao existe na base de dados! ";


            if (apagarOK && existeOK)
            {
                
                apagouOK = dal.ApagarColaboradorSingle(matricula);
                if (apagouOK) textoLabel = "apagou o colaborador com a matricula " + matricula.ToString();
                else textoLabel = "nao conseguiu apagar este colaborador!";
            }
            else 
            {
                textoLabel += "Não pode apagar este colaborador.";
            }
            
            
            labelApagar.Text = textoLabel;
        
        
        }

        private void botaoPesquisar_Click(object sender, EventArgs e)
        {
            if (textBoxMatriculaPesquisar.Text == "" || textBoxMatriculaPesquisar.Text == null)
            {
                labelMatriculaPesquisar.Text = "insira uma matricula valida.";
            }
            else
            {
                uint matriculaPesquisar = uint.Parse(textBoxMatriculaPesquisar.Text);

                DAL dal = new DAL();
                Colaborador colaborador = dal.SelectColaboradorSingle(matriculaPesquisar);
                
                if(colaborador==null) 
                { 
                    labelMatriculaPesquisar.Text = "Colaborador com esta matricula nao foi encontrado."; 
                }
                else
                {
                    labelMatriculaPesquisar.Text = "Colaborador encontrado.";

                    labelMostraNomeColaborador.Text = colaborador.GetNomeColaborador();
                    labelMostraLoginColaborador.Text = colaborador.GetLoginColaborador();
                }
            }

        }
    }

}
