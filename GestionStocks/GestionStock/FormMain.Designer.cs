﻿namespace GestionStock
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabMainPessoas = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabColaboradores = new System.Windows.Forms.TabPage();
            this.tabFuncoes = new System.Windows.Forms.TabPage();
            this.labelInstruccoes = new System.Windows.Forms.Label();
            this.labelApagarInfo = new System.Windows.Forms.Label();
            this.botaoApagarOK = new System.Windows.Forms.Button();
            this.labelFuncaoDesignacao = new System.Windows.Forms.Label();
            this.labelFuncaoID = new System.Windows.Forms.Label();
            this.DGVFuncao = new System.Windows.Forms.DataGridView();
            this.labelEntradas = new System.Windows.Forms.Label();
            this.botaoActualizar = new System.Windows.Forms.Button();
            this.botaoLimpar = new System.Windows.Forms.Button();
            this.botaoOK = new System.Windows.Forms.Button();
            this.botaoApagar = new System.Windows.Forms.Button();
            this.botaoEditar = new System.Windows.Forms.Button();
            this.botaoAdicionar = new System.Windows.Forms.Button();
            this.botaoPesquisar = new System.Windows.Forms.Button();
            this.textBoxEntradas = new System.Windows.Forms.TextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.funcaoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gestion_StockDataSet = new GestionStock.Gestion_StockDataSet();
            this.funcaoTableAdapter = new GestionStock.Gestion_StockDataSetTableAdapters.FuncaoTableAdapter();
            this.labelInstruccoesApagar = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabMainPessoas.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabFuncoes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVFuncao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.funcaoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gestion_StockDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabMainPessoas);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1171, 705);
            this.tabControl1.TabIndex = 0;
            // 
            // tabMainPessoas
            // 
            this.tabMainPessoas.Controls.Add(this.tabControl2);
            this.tabMainPessoas.Location = new System.Drawing.Point(4, 22);
            this.tabMainPessoas.Name = "tabMainPessoas";
            this.tabMainPessoas.Padding = new System.Windows.Forms.Padding(3);
            this.tabMainPessoas.Size = new System.Drawing.Size(1163, 679);
            this.tabMainPessoas.TabIndex = 0;
            this.tabMainPessoas.Text = "Pessoas";
            this.tabMainPessoas.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabColaboradores);
            this.tabControl2.Controls.Add(this.tabFuncoes);
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Location = new System.Drawing.Point(4, 7);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1159, 669);
            this.tabControl2.TabIndex = 0;
            // 
            // tabColaboradores
            // 
            this.tabColaboradores.Location = new System.Drawing.Point(4, 22);
            this.tabColaboradores.Name = "tabColaboradores";
            this.tabColaboradores.Padding = new System.Windows.Forms.Padding(3);
            this.tabColaboradores.Size = new System.Drawing.Size(1151, 643);
            this.tabColaboradores.TabIndex = 0;
            this.tabColaboradores.Text = "Colaboradores";
            this.tabColaboradores.UseVisualStyleBackColor = true;
            // 
            // tabFuncoes
            // 
            this.tabFuncoes.Controls.Add(this.labelInstruccoesApagar);
            this.tabFuncoes.Controls.Add(this.labelInstruccoes);
            this.tabFuncoes.Controls.Add(this.labelApagarInfo);
            this.tabFuncoes.Controls.Add(this.botaoApagarOK);
            this.tabFuncoes.Controls.Add(this.labelFuncaoDesignacao);
            this.tabFuncoes.Controls.Add(this.labelFuncaoID);
            this.tabFuncoes.Controls.Add(this.DGVFuncao);
            this.tabFuncoes.Controls.Add(this.labelEntradas);
            this.tabFuncoes.Controls.Add(this.botaoActualizar);
            this.tabFuncoes.Controls.Add(this.botaoLimpar);
            this.tabFuncoes.Controls.Add(this.botaoOK);
            this.tabFuncoes.Controls.Add(this.botaoApagar);
            this.tabFuncoes.Controls.Add(this.botaoEditar);
            this.tabFuncoes.Controls.Add(this.botaoAdicionar);
            this.tabFuncoes.Controls.Add(this.botaoPesquisar);
            this.tabFuncoes.Controls.Add(this.textBoxEntradas);
            this.tabFuncoes.Location = new System.Drawing.Point(4, 22);
            this.tabFuncoes.Name = "tabFuncoes";
            this.tabFuncoes.Padding = new System.Windows.Forms.Padding(3);
            this.tabFuncoes.Size = new System.Drawing.Size(1151, 643);
            this.tabFuncoes.TabIndex = 1;
            this.tabFuncoes.Text = "Funcoes";
            this.tabFuncoes.UseVisualStyleBackColor = true;
            // 
            // labelInstruccoes
            // 
            this.labelInstruccoes.AutoSize = true;
            this.labelInstruccoes.Location = new System.Drawing.Point(219, 77);
            this.labelInstruccoes.Name = "labelInstruccoes";
            this.labelInstruccoes.Size = new System.Drawing.Size(0, 13);
            this.labelInstruccoes.TabIndex = 17;
            this.labelInstruccoes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInstruccoes.Visible = false;
            // 
            // labelApagarInfo
            // 
            this.labelApagarInfo.AutoSize = true;
            this.labelApagarInfo.Location = new System.Drawing.Point(852, 303);
            this.labelApagarInfo.Name = "labelApagarInfo";
            this.labelApagarInfo.Size = new System.Drawing.Size(0, 13);
            this.labelApagarInfo.TabIndex = 16;
            this.labelApagarInfo.Click += new System.EventHandler(this.label1_Click);
            // 
            // botaoApagarOK
            // 
            this.botaoApagarOK.Location = new System.Drawing.Point(989, 277);
            this.botaoApagarOK.Name = "botaoApagarOK";
            this.botaoApagarOK.Size = new System.Drawing.Size(93, 23);
            this.botaoApagarOK.TabIndex = 15;
            this.botaoApagarOK.Text = "Apagar Funcao";
            this.botaoApagarOK.UseVisualStyleBackColor = true;
            this.botaoApagarOK.Click += new System.EventHandler(this.botaoApagarOK_Click);
            // 
            // labelFuncaoDesignacao
            // 
            this.labelFuncaoDesignacao.AutoSize = true;
            this.labelFuncaoDesignacao.Location = new System.Drawing.Point(481, 123);
            this.labelFuncaoDesignacao.Name = "labelFuncaoDesignacao";
            this.labelFuncaoDesignacao.Size = new System.Drawing.Size(0, 13);
            this.labelFuncaoDesignacao.TabIndex = 14;
            // 
            // labelFuncaoID
            // 
            this.labelFuncaoID.AutoSize = true;
            this.labelFuncaoID.Location = new System.Drawing.Point(481, 97);
            this.labelFuncaoID.Name = "labelFuncaoID";
            this.labelFuncaoID.Size = new System.Drawing.Size(0, 13);
            this.labelFuncaoID.TabIndex = 13;
            // 
            // DGVFuncao
            // 
            this.DGVFuncao.AllowUserToAddRows = false;
            this.DGVFuncao.AllowUserToDeleteRows = false;
            this.DGVFuncao.AllowUserToOrderColumns = true;
            this.DGVFuncao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVFuncao.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DGVFuncao.Location = new System.Drawing.Point(842, 97);
            this.DGVFuncao.MultiSelect = false;
            this.DGVFuncao.Name = "DGVFuncao";
            this.DGVFuncao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVFuncao.Size = new System.Drawing.Size(240, 150);
            this.DGVFuncao.TabIndex = 12;
            this.DGVFuncao.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVFuncao_CellContentClick);
            // 
            // labelEntradas
            // 
            this.labelEntradas.AutoSize = true;
            this.labelEntradas.Location = new System.Drawing.Point(219, 184);
            this.labelEntradas.Name = "labelEntradas";
            this.labelEntradas.Size = new System.Drawing.Size(0, 13);
            this.labelEntradas.TabIndex = 11;
            // 
            // botaoActualizar
            // 
            this.botaoActualizar.Location = new System.Drawing.Point(32, 277);
            this.botaoActualizar.Name = "botaoActualizar";
            this.botaoActualizar.Size = new System.Drawing.Size(75, 23);
            this.botaoActualizar.TabIndex = 9;
            this.botaoActualizar.Text = "Actualizar";
            this.botaoActualizar.UseVisualStyleBackColor = true;
            // 
            // botaoLimpar
            // 
            this.botaoLimpar.Location = new System.Drawing.Point(321, 140);
            this.botaoLimpar.Name = "botaoLimpar";
            this.botaoLimpar.Size = new System.Drawing.Size(75, 23);
            this.botaoLimpar.TabIndex = 7;
            this.botaoLimpar.Text = "Limpar";
            this.botaoLimpar.UseVisualStyleBackColor = true;
            this.botaoLimpar.Click += new System.EventHandler(this.botaoLimpar_Click);
            // 
            // botaoOK
            // 
            this.botaoOK.Location = new System.Drawing.Point(222, 139);
            this.botaoOK.Name = "botaoOK";
            this.botaoOK.Size = new System.Drawing.Size(75, 23);
            this.botaoOK.TabIndex = 6;
            this.botaoOK.Text = "OK";
            this.botaoOK.UseVisualStyleBackColor = true;
            this.botaoOK.Click += new System.EventHandler(this.botaoOK_Click);
            // 
            // botaoApagar
            // 
            this.botaoApagar.Location = new System.Drawing.Point(32, 233);
            this.botaoApagar.Name = "botaoApagar";
            this.botaoApagar.Size = new System.Drawing.Size(75, 23);
            this.botaoApagar.TabIndex = 5;
            this.botaoApagar.Text = "Apagar";
            this.botaoApagar.UseVisualStyleBackColor = true;
            this.botaoApagar.Click += new System.EventHandler(this.botaoApagar_Click);
            // 
            // botaoEditar
            // 
            this.botaoEditar.Location = new System.Drawing.Point(32, 184);
            this.botaoEditar.Name = "botaoEditar";
            this.botaoEditar.Size = new System.Drawing.Size(75, 23);
            this.botaoEditar.TabIndex = 4;
            this.botaoEditar.Text = "Editar";
            this.botaoEditar.UseVisualStyleBackColor = true;
            this.botaoEditar.Click += new System.EventHandler(this.botaoEditar_Click);
            // 
            // botaoAdicionar
            // 
            this.botaoAdicionar.Location = new System.Drawing.Point(32, 139);
            this.botaoAdicionar.Name = "botaoAdicionar";
            this.botaoAdicionar.Size = new System.Drawing.Size(75, 23);
            this.botaoAdicionar.TabIndex = 3;
            this.botaoAdicionar.Text = "Adicionar";
            this.botaoAdicionar.UseVisualStyleBackColor = true;
            this.botaoAdicionar.Click += new System.EventHandler(this.botaoAdicionar_Click);
            // 
            // botaoPesquisar
            // 
            this.botaoPesquisar.Location = new System.Drawing.Point(32, 97);
            this.botaoPesquisar.Name = "botaoPesquisar";
            this.botaoPesquisar.Size = new System.Drawing.Size(75, 23);
            this.botaoPesquisar.TabIndex = 2;
            this.botaoPesquisar.Text = "Pesquisar";
            this.botaoPesquisar.UseVisualStyleBackColor = true;
            this.botaoPesquisar.Click += new System.EventHandler(this.botaoPesquisar_Click);
            // 
            // textBoxEntradas
            // 
            this.textBoxEntradas.Location = new System.Drawing.Point(222, 97);
            this.textBoxEntradas.Name = "textBoxEntradas";
            this.textBoxEntradas.Size = new System.Drawing.Size(174, 20);
            this.textBoxEntradas.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1151, 643);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1163, 679);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // funcaoBindingSource
            // 
            this.funcaoBindingSource.DataMember = "Funcao";
            this.funcaoBindingSource.DataSource = this.gestion_StockDataSet;
            // 
            // gestion_StockDataSet
            // 
            this.gestion_StockDataSet.DataSetName = "Gestion_StockDataSet";
            this.gestion_StockDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // funcaoTableAdapter
            // 
            this.funcaoTableAdapter.ClearBeforeFill = true;
            // 
            // labelInstruccoesApagar
            // 
            this.labelInstruccoesApagar.AutoSize = true;
            this.labelInstruccoesApagar.Location = new System.Drawing.Point(950, 77);
            this.labelInstruccoesApagar.Name = "labelInstruccoesApagar";
            this.labelInstruccoesApagar.Size = new System.Drawing.Size(0, 13);
            this.labelInstruccoesApagar.TabIndex = 18;
            this.labelInstruccoesApagar.Visible = false;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1172, 720);
            this.Controls.Add(this.tabControl1);
            this.Name = "FormMain";
            this.Text = "Gestão de Stocks";
            this.tabControl1.ResumeLayout(false);
            this.tabMainPessoas.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabFuncoes.ResumeLayout(false);
            this.tabFuncoes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVFuncao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.funcaoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gestion_StockDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabMainPessoas;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabFuncoes;
        private System.Windows.Forms.TabPage tabColaboradores;
        private System.Windows.Forms.TabPage tabPage1;
       
    
       
       
        private System.Windows.Forms.Button botaoActualizar;
        private System.Windows.Forms.Button botaoLimpar;
        private System.Windows.Forms.Button botaoOK;
        private System.Windows.Forms.Button botaoApagar;
        private System.Windows.Forms.Button botaoEditar;
        private System.Windows.Forms.Button botaoAdicionar;
        private System.Windows.Forms.Button botaoPesquisar;
        private System.Windows.Forms.TextBox textBoxEntradas;
        private System.Windows.Forms.Label labelEntradas;
        private System.Windows.Forms.DataGridView DGVFuncao;
        private Gestion_StockDataSet gestion_StockDataSet;
        private System.Windows.Forms.BindingSource funcaoBindingSource;
        private Gestion_StockDataSetTableAdapters.FuncaoTableAdapter funcaoTableAdapter;
        private System.Windows.Forms.Label labelFuncaoDesignacao;
        private System.Windows.Forms.Label labelFuncaoID;
        private System.Windows.Forms.Button botaoApagarOK;
        private System.Windows.Forms.Label labelApagarInfo;
        private System.Windows.Forms.Label labelInstruccoes;
        private System.Windows.Forms.Label labelInstruccoesApagar;
    }
}