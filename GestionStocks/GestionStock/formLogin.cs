﻿using Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace GestionStock
{
    public partial class formLogin : Form
    {
        public formLogin()
        {
            InitializeComponent();
        }

        private void botaoLogin_Click(object sender, EventArgs e)
        {
            string utilizadorUserName = textBoxUtilizador.Text;
            string password = textBoxPassword.Text;
            bool loginOK = false;

            loginOK = BLL.VerificarLoginOK(utilizadorUserName, password);

            if (loginOK)
            {
                MDIMenu menu = new MDIMenu(utilizadorUserName);
                menu.Show();
                this.Hide();
            }
            else
            {    
                textBoxUtilizador.Text = "";
                textBoxPassword.Text = "";
                labelErroLogin.Text = "Erro no login!";
            }
        }
    }
}
