﻿namespace GestionStock
{
    partial class TesteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.botao1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.gestion_StockDataSet = new GestionStock.Gestion_StockDataSet();
            this.gestionStockDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.carregar2 = new System.Windows.Forms.Button();
            this.botaoAdicionar = new System.Windows.Forms.Button();
            this.textBoxMatricula = new System.Windows.Forms.TextBox();
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.textBoxNome = new System.Windows.Forms.TextBox();
            this.botaoConfirmaAdicionar = new System.Windows.Forms.Button();
            this.labelAdicionar = new System.Windows.Forms.Label();
            this.labelMatricula = new System.Windows.Forms.Label();
            this.labelLogin = new System.Windows.Forms.Label();
            this.labelNome = new System.Windows.Forms.Label();
            this.botaoApagar = new System.Windows.Forms.Button();
            this.botaoEditar = new System.Windows.Forms.Button();
            this.botaoApagarOK = new System.Windows.Forms.Button();
            this.botaoEditarOK = new System.Windows.Forms.Button();
            this.labelColaborador = new System.Windows.Forms.Label();
            this.botaoPesquisar = new System.Windows.Forms.Button();
            this.labelMostraColaborador = new System.Windows.Forms.Label();
            this.labelMostraNomeColaborador = new System.Windows.Forms.Label();
            this.textBoxMatriculaPesquisar = new System.Windows.Forms.TextBox();
            this.labelMatriculaColaboradorPesquisar = new System.Windows.Forms.Label();
            this.labelMostraLoginColaborador = new System.Windows.Forms.Label();
            this.labelMatriculaPesquisar = new System.Windows.Forms.Label();
            this.labelApagar = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gestion_StockDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gestionStockDataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // botao1
            // 
            this.botao1.Location = new System.Drawing.Point(116, 90);
            this.botao1.Name = "botao1";
            this.botao1.Size = new System.Drawing.Size(75, 23);
            this.botao1.TabIndex = 0;
            this.botao1.Text = "carregar dados";
            this.botao1.UseVisualStyleBackColor = true;
            this.botao1.Click += new System.EventHandler(this.botao1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(86, 204);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(553, 210);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseClick);
            // 
            // gestion_StockDataSet
            // 
            this.gestion_StockDataSet.DataSetName = "Gestion_StockDataSet";
            this.gestion_StockDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gestionStockDataSetBindingSource
            // 
            this.gestionStockDataSetBindingSource.DataSource = this.gestion_StockDataSet;
            this.gestionStockDataSetBindingSource.Position = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(116, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(256, 22);
            this.label1.TabIndex = 2;
            this.label1.Text = "Carregar dados da BD access!";
            // 
            // carregar2
            // 
            this.carregar2.Location = new System.Drawing.Point(218, 89);
            this.carregar2.Name = "carregar2";
            this.carregar2.Size = new System.Drawing.Size(75, 23);
            this.carregar2.TabIndex = 3;
            this.carregar2.Text = "carregar2";
            this.carregar2.UseVisualStyleBackColor = true;
            this.carregar2.Click += new System.EventHandler(this.carregar2_Click);
            // 
            // botaoAdicionar
            // 
            this.botaoAdicionar.Location = new System.Drawing.Point(321, 89);
            this.botaoAdicionar.Name = "botaoAdicionar";
            this.botaoAdicionar.Size = new System.Drawing.Size(75, 23);
            this.botaoAdicionar.TabIndex = 4;
            this.botaoAdicionar.Text = "Adicionar";
            this.botaoAdicionar.UseVisualStyleBackColor = true;
            this.botaoAdicionar.Click += new System.EventHandler(this.botaoAdicionar_Click);
            // 
            // textBoxMatricula
            // 
            this.textBoxMatricula.Location = new System.Drawing.Point(776, 116);
            this.textBoxMatricula.Name = "textBoxMatricula";
            this.textBoxMatricula.Size = new System.Drawing.Size(164, 20);
            this.textBoxMatricula.TabIndex = 5;
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.Location = new System.Drawing.Point(776, 175);
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(164, 20);
            this.textBoxLogin.TabIndex = 6;
            // 
            // textBoxNome
            // 
            this.textBoxNome.Location = new System.Drawing.Point(776, 229);
            this.textBoxNome.Name = "textBoxNome";
            this.textBoxNome.Size = new System.Drawing.Size(164, 20);
            this.textBoxNome.TabIndex = 7;
            // 
            // botaoConfirmaAdicionar
            // 
            this.botaoConfirmaAdicionar.Location = new System.Drawing.Point(776, 267);
            this.botaoConfirmaAdicionar.Name = "botaoConfirmaAdicionar";
            this.botaoConfirmaAdicionar.Size = new System.Drawing.Size(75, 23);
            this.botaoConfirmaAdicionar.TabIndex = 8;
            this.botaoConfirmaAdicionar.Text = "AdicionarOK";
            this.botaoConfirmaAdicionar.UseVisualStyleBackColor = true;
            this.botaoConfirmaAdicionar.Click += new System.EventHandler(this.botaoConfirmaAdicionar_Click);
            // 
            // labelAdicionar
            // 
            this.labelAdicionar.AutoSize = true;
            this.labelAdicionar.Location = new System.Drawing.Point(773, 307);
            this.labelAdicionar.Name = "labelAdicionar";
            this.labelAdicionar.Size = new System.Drawing.Size(50, 13);
            this.labelAdicionar.TabIndex = 9;
            this.labelAdicionar.Text = "adicionar";
            // 
            // labelMatricula
            // 
            this.labelMatricula.AutoSize = true;
            this.labelMatricula.Location = new System.Drawing.Point(773, 100);
            this.labelMatricula.Name = "labelMatricula";
            this.labelMatricula.Size = new System.Drawing.Size(125, 13);
            this.labelMatricula.TabIndex = 10;
            this.labelMatricula.Text = "Matricula do Colaborador";
            this.labelMatricula.Click += new System.EventHandler(this.label2_Click);
            // 
            // labelLogin
            // 
            this.labelLogin.AutoSize = true;
            this.labelLogin.Location = new System.Drawing.Point(773, 159);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(108, 13);
            this.labelLogin.TabIndex = 11;
            this.labelLogin.Text = "Login do Colaborador";
            // 
            // labelNome
            // 
            this.labelNome.AutoSize = true;
            this.labelNome.Location = new System.Drawing.Point(773, 213);
            this.labelNome.Name = "labelNome";
            this.labelNome.Size = new System.Drawing.Size(110, 13);
            this.labelNome.TabIndex = 12;
            this.labelNome.Text = "Nome do Colaborador";
            // 
            // botaoApagar
            // 
            this.botaoApagar.Location = new System.Drawing.Point(417, 89);
            this.botaoApagar.Name = "botaoApagar";
            this.botaoApagar.Size = new System.Drawing.Size(75, 23);
            this.botaoApagar.TabIndex = 13;
            this.botaoApagar.Text = "apagar";
            this.botaoApagar.UseVisualStyleBackColor = true;
            this.botaoApagar.Click += new System.EventHandler(this.botaoApagar_Click);
            // 
            // botaoEditar
            // 
            this.botaoEditar.Location = new System.Drawing.Point(517, 89);
            this.botaoEditar.Name = "botaoEditar";
            this.botaoEditar.Size = new System.Drawing.Size(75, 23);
            this.botaoEditar.TabIndex = 14;
            this.botaoEditar.Text = "Editar";
            this.botaoEditar.UseVisualStyleBackColor = true;
            // 
            // botaoApagarOK
            // 
            this.botaoApagarOK.Location = new System.Drawing.Point(858, 267);
            this.botaoApagarOK.Name = "botaoApagarOK";
            this.botaoApagarOK.Size = new System.Drawing.Size(75, 23);
            this.botaoApagarOK.TabIndex = 15;
            this.botaoApagarOK.Text = "ApagarOK";
            this.botaoApagarOK.UseVisualStyleBackColor = true;
            this.botaoApagarOK.Click += new System.EventHandler(this.botaoApagarOK_Click);
            // 
            // botaoEditarOK
            // 
            this.botaoEditarOK.Location = new System.Drawing.Point(940, 267);
            this.botaoEditarOK.Name = "botaoEditarOK";
            this.botaoEditarOK.Size = new System.Drawing.Size(75, 23);
            this.botaoEditarOK.TabIndex = 16;
            this.botaoEditarOK.Text = "EditarOK";
            this.botaoEditarOK.UseVisualStyleBackColor = true;
            // 
            // labelColaborador
            // 
            this.labelColaborador.AutoSize = true;
            this.labelColaborador.Location = new System.Drawing.Point(776, 71);
            this.labelColaborador.Name = "labelColaborador";
            this.labelColaborador.Size = new System.Drawing.Size(98, 13);
            this.labelColaborador.TabIndex = 17;
            this.labelColaborador.Text = "Dados Colaborador";
            // 
            // botaoPesquisar
            // 
            this.botaoPesquisar.Location = new System.Drawing.Point(931, 410);
            this.botaoPesquisar.Name = "botaoPesquisar";
            this.botaoPesquisar.Size = new System.Drawing.Size(75, 23);
            this.botaoPesquisar.TabIndex = 18;
            this.botaoPesquisar.Text = "pesquisar";
            this.botaoPesquisar.UseVisualStyleBackColor = true;
            this.botaoPesquisar.Click += new System.EventHandler(this.botaoPesquisar_Click);
            // 
            // labelMostraColaborador
            // 
            this.labelMostraColaborador.AutoSize = true;
            this.labelMostraColaborador.Location = new System.Drawing.Point(791, 500);
            this.labelMostraColaborador.Name = "labelMostraColaborador";
            this.labelMostraColaborador.Size = new System.Drawing.Size(110, 13);
            this.labelMostraColaborador.TabIndex = 19;
            this.labelMostraColaborador.Text = "dados do colaborador";
            // 
            // labelMostraNomeColaborador
            // 
            this.labelMostraNomeColaborador.AutoSize = true;
            this.labelMostraNomeColaborador.Location = new System.Drawing.Point(794, 532);
            this.labelMostraNomeColaborador.Name = "labelMostraNomeColaborador";
            this.labelMostraNomeColaborador.Size = new System.Drawing.Size(33, 13);
            this.labelMostraNomeColaborador.TabIndex = 20;
            this.labelMostraNomeColaborador.Text = "nome";
            // 
            // textBoxMatriculaPesquisar
            // 
            this.textBoxMatriculaPesquisar.Location = new System.Drawing.Point(744, 410);
            this.textBoxMatriculaPesquisar.Name = "textBoxMatriculaPesquisar";
            this.textBoxMatriculaPesquisar.Size = new System.Drawing.Size(129, 20);
            this.textBoxMatriculaPesquisar.TabIndex = 21;
            // 
            // labelMatriculaColaboradorPesquisar
            // 
            this.labelMatriculaColaboradorPesquisar.AutoSize = true;
            this.labelMatriculaColaboradorPesquisar.Location = new System.Drawing.Point(744, 391);
            this.labelMatriculaColaboradorPesquisar.Name = "labelMatriculaColaboradorPesquisar";
            this.labelMatriculaColaboradorPesquisar.Size = new System.Drawing.Size(106, 13);
            this.labelMatriculaColaboradorPesquisar.TabIndex = 22;
            this.labelMatriculaColaboradorPesquisar.Text = "matricula a pesquisar";
            // 
            // labelMostraLoginColaborador
            // 
            this.labelMostraLoginColaborador.AutoSize = true;
            this.labelMostraLoginColaborador.Location = new System.Drawing.Point(794, 563);
            this.labelMostraLoginColaborador.Name = "labelMostraLoginColaborador";
            this.labelMostraLoginColaborador.Size = new System.Drawing.Size(29, 13);
            this.labelMostraLoginColaborador.TabIndex = 23;
            this.labelMostraLoginColaborador.Text = "login";
            // 
            // labelMatriculaPesquisar
            // 
            this.labelMatriculaPesquisar.AutoSize = true;
            this.labelMatriculaPesquisar.Location = new System.Drawing.Point(744, 437);
            this.labelMatriculaPesquisar.Name = "labelMatriculaPesquisar";
            this.labelMatriculaPesquisar.Size = new System.Drawing.Size(0, 13);
            this.labelMatriculaPesquisar.TabIndex = 24;
            // 
            // labelApagar
            // 
            this.labelApagar.AutoSize = true;
            this.labelApagar.Location = new System.Drawing.Point(776, 324);
            this.labelApagar.Name = "labelApagar";
            this.labelApagar.Size = new System.Drawing.Size(40, 13);
            this.labelApagar.TabIndex = 25;
            this.labelApagar.Text = "apagar";
            // 
            // TesteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1087, 623);
            this.Controls.Add(this.labelApagar);
            this.Controls.Add(this.labelMatriculaPesquisar);
            this.Controls.Add(this.labelMostraLoginColaborador);
            this.Controls.Add(this.labelMatriculaColaboradorPesquisar);
            this.Controls.Add(this.textBoxMatriculaPesquisar);
            this.Controls.Add(this.labelMostraNomeColaborador);
            this.Controls.Add(this.labelMostraColaborador);
            this.Controls.Add(this.botaoPesquisar);
            this.Controls.Add(this.labelColaborador);
            this.Controls.Add(this.botaoEditarOK);
            this.Controls.Add(this.botaoApagarOK);
            this.Controls.Add(this.botaoEditar);
            this.Controls.Add(this.botaoApagar);
            this.Controls.Add(this.labelNome);
            this.Controls.Add(this.labelLogin);
            this.Controls.Add(this.labelMatricula);
            this.Controls.Add(this.labelAdicionar);
            this.Controls.Add(this.botaoConfirmaAdicionar);
            this.Controls.Add(this.textBoxNome);
            this.Controls.Add(this.textBoxLogin);
            this.Controls.Add(this.textBoxMatricula);
            this.Controls.Add(this.botaoAdicionar);
            this.Controls.Add(this.carregar2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.botao1);
            this.Name = "TesteForm";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gestion_StockDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gestionStockDataSetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button botao1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource gestionStockDataSetBindingSource;
        private Gestion_StockDataSet gestion_StockDataSet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button carregar2;
        private System.Windows.Forms.Button botaoAdicionar;
        private System.Windows.Forms.TextBox textBoxMatricula;
        private System.Windows.Forms.TextBox textBoxLogin;
        private System.Windows.Forms.TextBox textBoxNome;
        private System.Windows.Forms.Button botaoConfirmaAdicionar;
        private System.Windows.Forms.Label labelAdicionar;
        private System.Windows.Forms.Label labelMatricula;
        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.Label labelNome;
        private System.Windows.Forms.Button botaoApagar;
        private System.Windows.Forms.Button botaoEditar;
        private System.Windows.Forms.Button botaoApagarOK;
        private System.Windows.Forms.Button botaoEditarOK;
        private System.Windows.Forms.Label labelColaborador;
        private System.Windows.Forms.Button botaoPesquisar;
        private System.Windows.Forms.Label labelMostraColaborador;
        private System.Windows.Forms.Label labelMostraNomeColaborador;
        private System.Windows.Forms.TextBox textBoxMatriculaPesquisar;
        private System.Windows.Forms.Label labelMatriculaColaboradorPesquisar;
        private System.Windows.Forms.Label labelMostraLoginColaborador;
        private System.Windows.Forms.Label labelMatriculaPesquisar;
        private System.Windows.Forms.Label labelApagar;
    }
}

