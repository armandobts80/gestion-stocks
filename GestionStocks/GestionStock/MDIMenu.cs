﻿using System;
using System.Windows.Forms;

namespace GestionStock
{
    public partial class MDIMenu : Form
    {
        private string utilizadorUserName;
        private string tipoUtilizador;
        private bool permissaoModificar;

        public MDIMenu(string utilizador)
        {
            InitializeComponent();
            this.utilizadorUserName = utilizador;

            //tipoUtilizador = BLL.VerificarUtilizadorTipo(utilizadorUserName);
            //permissaoModificar = BLL.VerificarUtilizadorPermissao(utilizadorUserName);

            labelUtilizador.Text = utilizador;

            labelTipoUtilizador.Text = tipoUtilizador;

            labelPermissao.Text = "permissao modificar " + permissaoModificar.ToString();


        }

        private void Colaboradores_Click(object sender, EventArgs e)
        {

        }

        private void funcoesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formFuncoes = new FormFuncoes();
            
            formFuncoes.TopLevel = false; //set it's TopLevel to false 
            
                     
            Controls.Add(formFuncoes);
            formFuncoes.Show();
            
            formFuncoes.BringToFront();
           
           






            //.Show(); //finally display it

            //use this it there are Controls over your form.

        }

        private void Colaboradores_Click_1(object sender, EventArgs e)
        {

        }

        private void funcoes_Click(object sender, EventArgs e)
        {
            Form formFuncoes = new FormFuncoes();

            formFuncoes.TopLevel = false; //set it's TopLevel to false 

            formFuncoes.MdiParent = this;


            Controls.Add(formFuncoes);
            formFuncoes.Show();
            formFuncoes.BringToFront();

        }
    }
}
