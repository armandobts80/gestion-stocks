﻿namespace GestionStock
{
    partial class MDIMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.labelUtilizador = new System.Windows.Forms.Label();
            this.labelTipoUtilizador = new System.Windows.Forms.Label();
            this.labelPermissao = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(632, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 431);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(632, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusLabel.Text = "Status";
            // 
            // labelUtilizador
            // 
            this.labelUtilizador.AutoSize = true;
            this.labelUtilizador.Location = new System.Drawing.Point(28, 45);
            this.labelUtilizador.Name = "labelUtilizador";
            this.labelUtilizador.Size = new System.Drawing.Size(48, 13);
            this.labelUtilizador.TabIndex = 4;
            this.labelUtilizador.Text = "utilizador";
            // 
            // labelTipoUtilizador
            // 
            this.labelTipoUtilizador.AutoSize = true;
            this.labelTipoUtilizador.Location = new System.Drawing.Point(28, 75);
            this.labelTipoUtilizador.Name = "labelTipoUtilizador";
            this.labelTipoUtilizador.Size = new System.Drawing.Size(67, 13);
            this.labelTipoUtilizador.TabIndex = 5;
            this.labelTipoUtilizador.Text = "tipoUtilizador";
            // 
            // labelPermissao
            // 
            this.labelPermissao.AutoSize = true;
            this.labelPermissao.Location = new System.Drawing.Point(28, 107);
            this.labelPermissao.Name = "labelPermissao";
            this.labelPermissao.Size = new System.Drawing.Size(54, 13);
            this.labelPermissao.TabIndex = 6;
            this.labelPermissao.Text = "permissao";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            // 
            // MDIMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(632, 453);
            this.Controls.Add(this.labelPermissao);
            this.Controls.Add(this.labelTipoUtilizador);
            this.Controls.Add(this.labelUtilizador);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MDIMenu";
            this.Text = "Gestion de Stock";
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Label labelUtilizador;
        private System.Windows.Forms.Label labelTipoUtilizador;
        private System.Windows.Forms.Label labelPermissao;
        private System.Windows.Forms.Timer timer1;
    }
}



