﻿using Library;
using System;
using System.Data;
using System.Windows.Forms;

namespace GestionStock
{
    public partial class FormMain : Form
    {
        public static bool pesquisar = false;
        public static bool adicionar = false;
        public static bool editar = false;
        public static Funcao funcaoNovaEditar;
        public FormMain()
        {
            InitializeComponent();

            botaoOK.Visible = false;
            botaoLimpar.Visible = false;
            textBoxEntradas.Visible = false;
            labelEntradas.Visible = false;
            botaoApagarOK.Visible = true;
            botaoApagarOK.Visible = false;

            this.PreencherDGVfuncao();
        }
         

        private void botaoPesquisar_Click(object sender, EventArgs e)
        {
            botaoOK.Visible = true;
            botaoLimpar.Visible = true;
            textBoxEntradas.Visible = true;
            labelEntradas.Visible = true;
            labelInstruccoes.Visible = true;

            labelInstruccoes.Text = "Insira uma Funcao de Colaborador a pesquisar.";

            pesquisar = true;
            adicionar = false;
            editar = false;


        }

        private void botaoLimpar_Click(object sender, EventArgs e)
        {
            textBoxEntradas.Text = "";
            labelFuncaoID.Text = "";
            labelFuncaoDesignacao.Text = "";
            DGVFuncao.ClearSelection();
            labelInstruccoesApagar.Visible = false;
            labelInstruccoesApagar.Text = "";
          


        }
        private void botaoOK_Click(object sender, EventArgs e)
        {
            if(adicionar==true)
            {
             
                bool  adicionouOK = false;

                if (textBoxEntradas.Text == "")
                {
                    labelEntradas.Text = "Caixa de pesquisa vazia. Insira texto a com designacao \n da funcao a adicionar."; 
                }
                else
                {
                    DAL dal = new DAL();
                    string funcaoAdicionar = textBoxEntradas.Text;
                    
                    Funcao funcaoAntigaCheck = dal.SelectFuncaoSinglePorDesignacao(funcaoAdicionar);

                    if (funcaoAntigaCheck == null)
                    {
                        labelEntradas.Text = "Funcao foi adicionada com sucesso.";
                        Funcao funcaoNova = new Funcao(funcaoAdicionar);
                        adicionouOK = dal.InsertFuncao(funcaoNova);
                        if (adicionouOK)
                        { 
                            labelEntradas.Text = $"Funcao com designacao  {funcaoAdicionar} foi adicionada com sucesso.";
                            PreencherDGVfuncao();
                        }
                        else 
                        {
                            labelEntradas.Text = "Erro! Funcao não foi gravada com sucesso."; 
                        }
                    }
                    else
                    {
                        labelEntradas.Text = $"Já existe uma funcao com a designacao: {funcaoAdicionar}.\nEscolha uma designacao diferente.";
                    }
                }
            }



            if (pesquisar == true)
            {
               
                if (textBoxEntradas.Text == "")
                { labelEntradas.Text = "Caixa de pesquisa vazia. Insira texto a pesquisar."; }
                else
                {
                    string funcaoPesquisar = textBoxEntradas.Text;
                    DAL dal = new DAL();
                    Funcao funcao = dal.SelectFuncaoSinglePorDesignacao(funcaoPesquisar);

                    if (funcao == null)
                    {
                        labelEntradas.Text = "Funcao não encontrada.";
                    }
                    else
                    {
                        labelEntradas.Text = "Foi encontrada a Funcao correspondente.";
                        labelFuncaoID.Text = funcao.GetFuncaoID().ToString();
                        labelFuncaoDesignacao.Text = funcao.GetFuncaoDesignacao();
                        DGVFuncao.ClearSelection();

                        foreach (DataGridViewRow linha in DGVFuncao.Rows)
                        {
                            int valor = (int)linha.Cells[0].Value;
                            if (valor == funcao.GetFuncaoID())
                                linha.Selected = true;
                        }
                    }
                }
            }

            if (editar == true)
            {
                string texto;
                uint id;
                bool editouOK = false;

                if(DGVFuncao.SelectedRows.Count!=0)
                {   
                    if (textBoxEntradas.Text == "")
                    {
                        labelEntradas.Text = "Caixa de texto vazia, insira a nova designacao da funcao a editar.";
                    }
                    else
                    {
                        texto = textBoxEntradas.Text; 
                        id = uint.Parse(DGVFuncao.SelectedRows[0].Cells[0].Value.ToString());
                        funcaoNovaEditar = new Funcao(id, texto);

                        DAL dal = new DAL();
                        string funcaoNova = funcaoNovaEditar.GetFuncaoDesignacao();

                        bool funcaoAntigaCheck = BLL.VerificarFuncaoDesignacaoExiste(funcaoNova);

                        if (!funcaoAntigaCheck)
                        {
                            editouOK = dal.UpdateFuncao(funcaoNovaEditar);

                            if (editouOK)
                            {
                                labelEntradas.Text = $"Funcao foi actualizada com sucesso com nova designacao  {funcaoNova}.";
                                PreencherDGVfuncao();
                            }
                            else
                            {
                                labelEntradas.Text = "Erro! Funcao não foi editada com sucesso.";
                            }
                        }
                        else
                        {
                            labelEntradas.Text = $"Já existe uma funcao com a designacao: {funcaoNova}.\nEscolha uma designacao diferente.";
                        }
                    }
                }
                else
                {
                    labelEntradas.Text ="Sem seleccao na tabela. Seleccione uma funcao a editar.";
                }
            }
        }

        private void botaoApagar_Click(object sender, EventArgs e)
        {
            botaoOK.Visible = false;
            botaoLimpar.Visible = false;
            textBoxEntradas.Visible = false;
            labelEntradas.Visible = false;

            textBoxEntradas.Text = "";
            labelFuncaoID.Text = "";
            labelFuncaoDesignacao.Text = "";
            DGVFuncao.ClearSelection();

            botaoApagarOK.Visible = true;
            labelInstruccoes.Visible = false;
            labelInstruccoesApagar.Visible = true;
            labelInstruccoesApagar.Text = "Seleccione a Funcao a apagar e clique Apagar.";
            

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void botaoApagarOK_Click(object sender, EventArgs e)
        {
            var linhaSeleccionada = DGVFuncao.SelectedRows;
            int linhaSeleccionadaIndex = 0;
            string funcao = "";
            uint funcaoID;
            bool apagou = false;
            bool apagarOK = false;

            if (linhaSeleccionada.Count == 0)
            { 
                labelApagarInfo.Text = "Deve seleccionar na tabela uma Funcao a apagar."; 
            }
            else
            {
                apagarOK = (MessageBox.Show("Quer mesmo apagar esta Funcao?", "Apagar Funcao?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes);

                if (apagarOK)
                {
                    linhaSeleccionadaIndex = linhaSeleccionada[0].Index;
                    funcao = linhaSeleccionada[0].Cells[1].Value.ToString();
                    funcaoID = uint.Parse(linhaSeleccionada[0].Cells[0].Value.ToString());
                    //apagarFuncaoOK=BLL.VerificaApagarFuncaoOK(funcaoID);
                    apagou = Funcao.ApagarFuncao(funcaoID);
                }

                if (apagou)
                {
                    labelApagarInfo.Text = $"O registo da Funcao '{funcao}' foi apagado.";
                    PreencherDGVfuncao();
                    DGVFuncao.ClearSelection();

                }
                else
                {
                    labelApagarInfo.Text = $"Erro. O registo da Funcao '{funcao}' foi NAO foi apagado. Houve um erro.";
                }
            }

        }


        private void PreencherDGVfuncao()
        {
            DAL dal = new DAL();
            DataTable dataTable = new DataTable();
            dataTable = dal.SelectDataTableFuncoes();
            DGVFuncao.DataSource = dataTable;
            DGVFuncao.Refresh();
        }

        private void botaoAdicionar_Click(object sender, EventArgs e)
        {
            textBoxEntradas.Text = "";
            textBoxEntradas.Visible = true;
            botaoOK.Visible = true;
            botaoLimpar.Visible = true;
            labelEntradas.Visible = true;
            adicionar = true;
            labelInstruccoes.Visible = true;

            pesquisar = false;
            adicionar = true;
            editar = false;

        }

        private void botaoEditar_Click(object sender, EventArgs e)
        {
            textBoxEntradas.Text = "";
            textBoxEntradas.Visible = true;
            botaoOK.Visible = true;
            botaoLimpar.Visible = true;
            labelEntradas.Visible = true;
            adicionar = true;
            labelInstruccoes.Visible = true;
            labelInstruccoes.Text = "Clique na tabela e escolha uma funcao a editar, insira na caixa de texto a nova designacao.";
            pesquisar = false;
            adicionar = false;
            editar = true;
        }

        private void DGVFuncao_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
            
        }
    }
}
