﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public static class BLL
    {
        #region Validar Nova Matricula de Colaborador
        public static bool ValidarNovaMatricula(uint novaMatricula)
        {
            DAL dal = new DAL();
            List<Colaborador> listaColaboradores = dal.SelectColaboradores();
            if (listaColaboradores != null)
            {
                foreach (Colaborador colaborador in listaColaboradores)
                {
                    if (colaborador.GetColaboradorMatricula() == novaMatricula)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        #endregion

       

        #region Ler a matricula maxima dos Colaborador
        public static uint MaxMatricula()
        {
            uint matriculaMaisAlta = 0;
            DAL dal = new DAL();
            List<Colaborador> listaColaboradores = dal.SelectColaboradores();
            if (listaColaboradores != null)
            {
                foreach (Colaborador colaborador in listaColaboradores)
                {
                    if (colaborador.GetColaboradorMatricula() > matriculaMaisAlta)
                    {
                        matriculaMaisAlta = colaborador.GetColaboradorMatricula();
                    }
                }
            }
            return matriculaMaisAlta;
        }
        #endregion

        //apenas permitir apagar se nao tem atribuicoes registadas de material
        #region Validar Apagar Colaborador
        public static bool ValidarApagarColaborador(uint matricula)
        {
            bool apagarOK = true;



            return apagarOK;
        }
        #endregion

        public static string VerificarUtilizadorServico(string utilizadorUserName)
        {   
            string utilizadorTipo = "ADMIN";
            return utilizadorTipo;
        }


        public static bool  VerificarUtilizadorPermissao(string utilizadorUserName)
        {
            return true;
        }

        public static bool VerificarLoginOK(string utilizadorUserName, string utilizadorPassword)
        {
            DAL dal = new DAL();
            bool loginOK = false;

            loginOK = dal.VerificarUtilizadorLoginPassword(utilizadorUserName, utilizadorPassword);
           

            return loginOK;
        }

        public static bool VerificarFuncaoDesignacaoExiste(string funcaoDesignacao)
        {
            DAL dal = new DAL();

            bool designacaoExiste = false;

            Funcao funcao = dal.SelectFuncaoSinglePorDesignacao(funcaoDesignacao);

            if (funcao != null) designacaoExiste = true;

            return designacaoExiste;
        }

    }
}
