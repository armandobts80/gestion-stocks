﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class CriadorComandoSQL
    {
        public static List<string> listaIDs = new List<string> {"Id","Funcao_ID"};
        public static OleDbCommand Select(Type tipoClasse, OleDbConnection Ligacao)
        {
            
            OleDbCommand comandoSQL = new OleDbCommand();
            comandoSQL.Connection = Ligacao;

            comandoSQL.CommandText = $"Select * From [{tipoClasse.Name}]";

            return comandoSQL;
        }

        public static OleDbCommand SelectSingle(Type tipoClasse, Object valor, string campoAlvo , OleDbConnection Ligacao)
        {

            OleDbCommand comandoSQL = new OleDbCommand();
            comandoSQL.Connection = Ligacao;

            string valorQuery = valor.ToString();

            if (valor is string) { valorQuery = "\'" + valor.ToString() + "\'"; }
            else if(valor is uint) { valorQuery = valor.ToString(); }

            comandoSQL.CommandText = $"Select * From [{tipoClasse.Name}] where {campoAlvo} = {valorQuery}";
           // comandoSQL.CommandText = comandoSQL.CommandText.Remove(comandoSQL.CommandText.Length - 1, 1);
           // comandoSQL.CommandText += $")";

            return comandoSQL;
        }


        public static OleDbCommand Insert(string nomeTabela, List<string> nomesColunas, OleDbConnection Ligacao)
        {
        
            OleDbCommand comando = new OleDbCommand();
        
            comando.Connection = Ligacao;
            comando.CommandText = $"Insert INTO [{nomeTabela}] (";
                             
            foreach (string nomeColuna in nomesColunas)
            {
                comando.CommandText += (!listaIDs.Contains(nomeColuna)) ? $"[{nomeColuna}]," : "";
            }
        
            comando.CommandText = comando.CommandText.Remove(comando.CommandText.Length - 1, 1);
            comando.CommandText += $") VALUES (";

            foreach (string nomeColuna in nomesColunas)
            {
                comando.CommandText += (!listaIDs.Contains(nomeColuna)) ? $"@{nomeColuna}," : "";
            }
        
            comando.CommandText = comando.CommandText.Remove(comando.CommandText.Length - 1, 1);
            comando.CommandText += $")";

            return comando;

        }

        public static OleDbCommand DeleteSingle(Type tipoClasse, string nomeIdentificador, OleDbConnection Ligacao)
        {

            OleDbCommand comandoSQL = new OleDbCommand();
            
            comandoSQL.Connection = Ligacao;
                      
            comandoSQL.CommandText = $"DELETE FROM [{tipoClasse.Name}] where {nomeIdentificador} = @{nomeIdentificador}";
            comandoSQL.CommandText = comandoSQL.CommandText.Remove(comandoSQL.CommandText.Length - 1, 1);
           // comandoSQL.CommandText += $")";
            return comandoSQL;
        }

        public static OleDbCommand DeleteAll(Type tipoClasse, OleDbConnection Ligacao)
        {

            OleDbCommand comandoSQL = new OleDbCommand();

            comandoSQL.Connection = Ligacao;

            comandoSQL.CommandText = $"DELETE * FROM [{tipoClasse.Name}]";
        
            // comandoSQL.CommandText += $")";
            return comandoSQL;
        }

        public static OleDbCommand UpdateSingleItemSingleValue(Type tipoClasse, string nomeIdentificador, string nomeValorMudar, OleDbConnection Ligacao)
        {
            OleDbCommand comando = new OleDbCommand();

            comando.Connection = Ligacao;
            comando.CommandText = $"Update [{tipoClasse.Name}] SET ";
            comando.CommandText += $"{nomeValorMudar}= @{nomeValorMudar}";
            comando.CommandText += " Where ";
            comando.CommandText += $"{nomeIdentificador}=@{nomeIdentificador}";
            return comando;
        }


        //public static OleDbCommand Delete(object objectToDelete, OleDbConnection connection)
        //{
        //    Type objType = objectToDelete.GetType();
        //    OleDbCommand command = new OleDbCommand();
        //    command.Connection = connection;

        //    command.CommandText = $"DELETE FROM [{objType.Name}] Where ";
        //    PropertyInfo[] props = objType.GetProperties();
        //    foreach (PropertyInfo prop in props)
        //    {
        //        command.CommandText += (prop.Name == "Id") ? $"{prop.Name} = @{prop.Name} " : "";
        //    }
        //    return command;
        //}

        //public static OleDbCommand Update(string nomeTabela, OleDbConnection Ligacao)
        //{
        //    Type objType = objectToUpdate.GetType();
        //    OleDbCommand command = new OleDbCommand();
        //    command.Connection = connection;

        //    command.CommandText = $"Update [{objType.Name}] SET ";
        //    PropertyInfo[] props = objType.GetProperties();
        //    foreach (PropertyInfo prop in props)
        //    {
        //        command.CommandText += (prop.Name != "Id") ? $"{prop.Name}=@{prop.Name} " : "";
        //        //if (prop.Name != "Id") command.CommandText += $"{prop.Name}=@{prop.Name},";
        //    }
        //    command.CommandText += " Where ";
        //    foreach (PropertyInfo prop in props)
        //    {
        //        command.CommandText += (prop.Name == "Id") ? $"{prop.Name}=@{prop.Name} " : "";
        //        /*if (prop.Name == "Id") command.CommandText += $"{prop.Name}=@{prop.Name} ";*/
        //    }
        //    command.CommandText.Remove(command.CommandText.Length - 1, 1);

        //    return command;
        //}
        //


    }
}

