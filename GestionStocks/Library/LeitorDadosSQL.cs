﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class LeitorDadosSQL
    {

        #region Atributos
        private OleDbDataReader leitorDados { get; set; }
        #endregion

        #region Constructores
        public LeitorDadosSQL()
        {

        }
        public LeitorDadosSQL(OleDbDataReader leitorDados)
        {
            this.leitorDados = leitorDados;
        }

        #endregion

        #region metodos
        public int LerInteiro32bits(int index)
        {
            bool invalido = false;
            invalido = this.leitorDados.IsDBNull(index);

            int valorValido;

            if (invalido)
            { valorValido = 0; }
            else
            { valorValido = leitorDados.GetInt32(index); }

            return valorValido;

        }
       
        public int LerInteiro32bits(string nomeColuna)
        {
            int index = leitorDados.GetOrdinal(nomeColuna);

            bool invalido = leitorDados.IsDBNull(index);
            
            int valorValido;

            if (invalido)
            { valorValido = 0; }
            else
            { valorValido = leitorDados.GetInt32(index); }

            return valorValido;

        }


        public uint LerInteiroUnsigned32bits(int index)
        {
            bool invalido = false;
            invalido = this.leitorDados.IsDBNull(index);

            uint valorValido;

            if (invalido)
            { valorValido = 0; }
            else
            { valorValido = (uint)leitorDados.GetInt32(index); }

            return valorValido;

        }

        public uint LerInteiroUnsigned32bits(string nomeColuna)
        {
            int index = this.leitorDados.GetOrdinal(nomeColuna);

            bool invalido = this.leitorDados.IsDBNull(index);

            uint valorValido;

            if (invalido)
            { valorValido = 0; }
            else
            { valorValido = (uint)leitorDados.GetInt32(index); }

            return valorValido;

        }




        public string LerString(int index)
        {
            bool invalido = false;
            invalido = this.leitorDados.IsDBNull(index);

            string stringValida;

            if (invalido)
            { stringValida = ""; }
            else
            { stringValida = leitorDados.GetString(index); }

            return stringValida;

        }

        public string LerString(string nomeColuna)
        {
            int index = leitorDados.GetOrdinal(nomeColuna);

            bool invalido = leitorDados.IsDBNull(index);

            string stringValida;

            if (invalido)
            { stringValida = ""; }
            else
            { stringValida = leitorDados.GetString(index); }

            return stringValida;

        }




        #endregion

        /*
            
            public DateTime GetDateTime(int index)
            {
                return dataR.IsDBNull(index) ? DateTime.Now : dataR.GetDateTime(index);
            }
            public DateTime GetDateTime(string columnName)
            {
                return dataR.IsDBNull(dataR.GetOrdinal(columnName)) ? DateTime.Now : dataR.GetDateTime(dataR.GetOrdinal(columnName));
            }
            public bool GetBool(int index)
            {
                return dataR.IsDBNull(index) ? false : dataR.GetBoolean(index);
            }
            public bool GetBool(string columnName)
            {
                return dataR.IsDBNull(dataR.GetOrdinal(columnName)) ? false : dataR.GetBoolean(dataR.GetOrdinal(columnName));
            }
            public double GetDouble(int index)
            {
                return dataR.IsDBNull(index) ? 0 : dataR.GetDouble(index);
            }
            public double GetDouble(string columnName)
            {
                return dataR.IsDBNull(dataR.GetOrdinal(columnName)) ? 0 : dataR.GetDouble(dataR.GetOrdinal(columnName));
            }
        }
    }
}

    */

    } 
}
