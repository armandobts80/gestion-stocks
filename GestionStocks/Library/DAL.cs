﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Library
{
    public class DAL
    {
        #region Campos
        public OleDbConnection Ligacao;
        #endregion
        #region Constructores
        public DAL()
        {
            string stringLigacao = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\Users\\Armando\\Desktop\\GestionStocks\\GestionStock\\bin\\Debug\\Gestion_Stock.accdb;Persist Security Info=True;Jet OLEDB:Database Password=stock@bts**";
            Ligacao = new OleDbConnection(stringLigacao);
        }
        public DAL(string stringLigacao)
        {
            Ligacao = new OleDbConnection(stringLigacao);
        }
        #endregion






        public List<Colaborador> SelectColaboradores()
        {
            if (Ligacao.State == ConnectionState.Closed)
            { Ligacao.Open(); }

            List<Colaborador> listaColaboradores = new List<Colaborador>();

            Type tipoColaborador = typeof(Colaborador);

            OleDbCommand comando = CriadorComandoSQL.Select(tipoColaborador, Ligacao);

            OleDbDataReader leitorDados = comando.ExecuteReader();

            if (leitorDados.HasRows)
            {
                LeitorDadosSQL leitor = new LeitorDadosSQL(leitorDados);

                while (leitorDados.Read())
                {

                    uint matricula = leitor.LerInteiroUnsigned32bits("Colaborador_Matricula");
                    string loginColaborador = leitor.LerString("Login_Colaborador");
                    string nomeColaborador = leitor.LerString("Nome_Colaborador");
                    uint funcaoColaboradorID = leitor.LerInteiroUnsigned32bits("Funcao");


                    Funcao funcao = SelectFuncaoSingle(funcaoColaboradorID);
                    Colaborador colaborador = new Colaborador(matricula, loginColaborador, nomeColaborador,funcao);

                    listaColaboradores.Add(colaborador);
                }
            }

            leitorDados.Close();

            Ligacao.Close();

            return listaColaboradores;
        }

        public DataTable SelectDataTableColaboradores()
        {
            if (Ligacao.State == ConnectionState.Closed)
            { Ligacao.Open(); }

            DataTable dataTable = new DataTable();
            Type tipoColaborador = typeof(Colaborador);
            OleDbCommand comando = CriadorComandoSQL.Select(tipoColaborador, Ligacao);
            OleDbDataReader dataReader = comando.ExecuteReader();
            dataTable.Load(dataReader);
            Ligacao.Close();
            return dataTable;
                     
        }

        public DataTable SelectDataTableFuncoes()
        {
            if (Ligacao.State == ConnectionState.Closed)
            { Ligacao.Open(); }

            DataTable dataTable = new DataTable();
            Type tipoFuncao = typeof(Funcao);
            OleDbCommand comando = CriadorComandoSQL.Select(tipoFuncao, Ligacao);
            OleDbDataReader dataReader = comando.ExecuteReader();
            dataTable.Load(dataReader);
            Ligacao.Close();
            return dataTable;

        }

        public Colaborador SelectColaboradorSingle(uint matriculaColaborador)
        {
            if (Ligacao.State == ConnectionState.Closed)
            { Ligacao.Open(); }

            Type tipoColaborador = typeof(Colaborador);

            string nomeCampo = "Colaborador_Matricula";

            Colaborador colaborador = new Colaborador(0,"0","0",null);

            OleDbCommand comando = CriadorComandoSQL.SelectSingle(tipoColaborador, matriculaColaborador , nomeCampo ,Ligacao);

            OleDbDataReader leitorDados = comando.ExecuteReader();

            if (leitorDados.HasRows)
            {
                LeitorDadosSQL leitor = new LeitorDadosSQL(leitorDados);

                //while (leitorDados.Read())
                //{
                if (leitorDados.Read())
                {
                    uint matricula = leitor.LerInteiroUnsigned32bits("Colaborador_Matricula");
                    string loginColaborador = leitor.LerString("Login_Colaborador");
                    string nomeColaborador = leitor.LerString("Nome_Colaborador");
                    uint funcaoColaboradorID = leitor.LerInteiroUnsigned32bits("Funcao");

                    Funcao funcaoColaborador = SelectFuncaoSingle(funcaoColaboradorID);
                    
                    colaborador.SetColaboradorMatricula(matricula);
                    colaborador.SetLoginColaborador(loginColaborador);
                    colaborador.SetNomeColaborador(nomeColaborador);
                    colaborador.SetFuncaoColaborador(funcaoColaborador);
                }
                    
                //}
            }

            else { colaborador = null; }

            leitorDados.Close();

            Ligacao.Close();

            return colaborador;
        }

        public Colaborador SelectColaboradorSinglePorLogin(string loginColaborador)
        {
            if (Ligacao.State == ConnectionState.Closed)
            { Ligacao.Open(); }

            Type tipoColaborador = typeof(Colaborador);

            string nomeCampo = "Colaborador_Matricula";

            Colaborador colaborador = new Colaborador(0, "0", "0", null);

            OleDbCommand comando = CriadorComandoSQL.SelectSingle(tipoColaborador, loginColaborador, nomeCampo, Ligacao);

            OleDbDataReader leitorDados = comando.ExecuteReader();

            if (leitorDados.HasRows)
            {
                LeitorDadosSQL leitor = new LeitorDadosSQL(leitorDados);

                //while (leitorDados.Read())
                //{
                if (leitorDados.Read())
                {
                    uint matricula = leitor.LerInteiroUnsigned32bits("Colaborador_Matricula");
                    string loginColaboradorNew = leitor.LerString("Login_Colaborador");
                    string nomeColaborador = leitor.LerString("Nome_Colaborador");
                    uint funcaoColaboradorID = leitor.LerInteiroUnsigned32bits("Funcao");

                    Funcao funcaoColaboradorNova = SelectFuncaoSingle(funcaoColaboradorID);

                    colaborador.SetColaboradorMatricula(matricula);
                    colaborador.SetLoginColaborador(loginColaboradorNew);
                    colaborador.SetNomeColaborador(nomeColaborador);
                    colaborador.SetFuncaoColaborador(funcaoColaboradorNova);
                }

                //}
            }

            else { colaborador = null; }

            leitorDados.Close();

            Ligacao.Close();

            return colaborador;
        }



        //modificar para inserri um objecto criado
        public bool InserirColaborador(uint matricula,string login,string nome, string funcao)
        {
            
            
            
            List<string> listaColunas = LerNomesColunasTabela("Colaborador");

            OleDbCommand comando = CriadorComandoSQL.Insert("Colaborador",listaColunas, Ligacao);

            comando.Parameters.AddWithValue("@" + listaColunas[0], matricula);
            comando.Parameters.AddWithValue("@" + listaColunas[1], login);
            comando.Parameters.AddWithValue("@" + listaColunas[2], nome);
            comando.Parameters.AddWithValue("@" + listaColunas[3], funcao);


            if (Ligacao.State != ConnectionState.Open) Ligacao.Open();


            int linhasInseridas = comando.ExecuteNonQuery();
                
            Ligacao.Close();

            bool retorno = (linhasInseridas > 0) ? true : false;
                
            return retorno;
                
            
        }

        public bool ApagarColaboradorSingle(uint matricula)
        {
            bool apagouOK = false;
            
            Type tipoColaborador = typeof(Colaborador);
            //ler da db idenditicador da tabela
            string nomeIdentificador = "COLABORADOR_MATRICULA";

            OleDbCommand comandoSQL = CriadorComandoSQL.DeleteSingle(tipoColaborador, nomeIdentificador, Ligacao);

            comandoSQL.Parameters.AddWithValue("@"+ nomeIdentificador, matricula);
                        
            if (Ligacao.State != ConnectionState.Open) Ligacao.Open();
            int linhaApagada = comandoSQL.ExecuteNonQuery();
            if(linhaApagada > 0) apagouOK = true;
            
            Ligacao.Close();
            return apagouOK;
        }

        public List<string> LerNomesColunasTabela(string nomeTabela)
        {
            if (!TabelaExiste(nomeTabela)) return null;
            
            List<string> nomesColunas = new List<string>();

            if (Ligacao.State != ConnectionState.Open) Ligacao.Open();

            DataTable esquemaTabela = Ligacao.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, new Object[] { null, null, nomeTabela });
            esquemaTabela.DefaultView.Sort = "ORDINAL_POSITION asc";
            esquemaTabela = esquemaTabela.DefaultView.ToTable();

            Ligacao.Close();

            if (esquemaTabela == null) return null;

            for (int i = 0; i < esquemaTabela.Columns.Count; i++)
            {   
                if(esquemaTabela.Columns[i].ColumnName == "COLUMN_NAME")
                {
                    for(int x=0; x<esquemaTabela.Rows.Count; x++)
                    {
                        string nomeCampo = (string)esquemaTabela.Rows[x].ItemArray[i];
                        nomesColunas.Add(nomeCampo);
                    }
                       
                    break;
                }
            }

            return nomesColunas;
            /*
            for (int j = 0; j < datatableinfo.Rows.Count; j++)
            {
                for (int i = 0; i < datatableinfo.Columns.Count; i++)
                {
                    Console.Write(datatableinfo.Columns[i].ColumnName + " ");
                    Console.WriteLine(datatableinfo.Rows[j].ItemArray[i] + " ");
                }
            }
            */


        }

        public bool TabelaExiste(string nomeTabela)
        {
            if (Ligacao.State != ConnectionState.Open) Ligacao.Open();

            DataTable tabelas = Ligacao.GetSchema("Tables");

            Ligacao.Close();

            bool tabelaExiste = false;

            for (var i = 0; i < tabelas.Rows.Count; i++)
            {
                tabelaExiste = String.Equals(tabelas.Rows[i][2].ToString(), nomeTabela, StringComparison.CurrentCultureIgnoreCase);
                
                if (tabelaExiste)
                    break;
            }
            return tabelaExiste;

            

        }

        public bool VerificarUtilizadorLoginPassword(string utilizadorLogin, string utilizadorPassword)
        {
            bool loginOK=false;

            if (Ligacao.State != ConnectionState.Open) Ligacao.Open();

            Type tipoUtilizador = typeof(Utilizador);

            string nomeCampo = "Login";
        
            OleDbCommand comando = CriadorComandoSQL.SelectSingle(tipoUtilizador, utilizadorLogin, nomeCampo, Ligacao);

            OleDbDataReader leitorDados = comando.ExecuteReader();

            if (leitorDados.HasRows)
            {
               LeitorDadosSQL leitor = new LeitorDadosSQL(leitorDados);
            
                if (leitorDados.Read())
                {
                    string login = leitor.LerString("Login");
                    string password = leitor.LerString("Password");
                    
                    if(login == utilizadorLogin && utilizadorPassword == password)
                    {
                        loginOK = true;
                    }
                    
                }

                leitorDados.Close();
                Ligacao.Close();
    
            }

            return loginOK;
        }


        public Funcao SelectFuncaoSingle(uint funcaoID)
        {
            if (Ligacao.State == ConnectionState.Closed)
            { Ligacao.Open(); }
            Type tipoFuncao = typeof(Funcao);
            string nomeCampo = "Funcao_ID";
            Funcao funcao = new Funcao();
            OleDbCommand comando = CriadorComandoSQL.SelectSingle(tipoFuncao, funcaoID, nomeCampo, Ligacao);
            OleDbDataReader leitorDados = comando.ExecuteReader();

            if (leitorDados.HasRows)
            {
                LeitorDadosSQL leitor = new LeitorDadosSQL(leitorDados);
                                
                if (leitorDados.Read())
                {
                    string funcaoDesignacao = leitor.LerString("Designacao");

                    funcao.SetFuncaoID(funcaoID);
                    funcao.SetFuncaoDesignacao(funcaoDesignacao);
                }
            }

            else { funcao = null; }
            leitorDados.Close();
            Ligacao.Close();

            return funcao;
        }

        public Funcao SelectFuncaoSinglePorDesignacao(string funcaoDesignacao)
        {     
            if (Ligacao.State == ConnectionState.Closed)
            { Ligacao.Open(); }
            Type tipoFuncao = typeof(Funcao);
            string nomeCampo = "Designacao";
            Funcao funcao = new Funcao();
            OleDbCommand comando = CriadorComandoSQL.SelectSingle(tipoFuncao, funcaoDesignacao, nomeCampo, Ligacao);
            OleDbDataReader leitorDados = comando.ExecuteReader();

            if (leitorDados.HasRows)
            {
                LeitorDadosSQL leitor = new LeitorDadosSQL(leitorDados);

                if (leitorDados.Read())
                {
                    uint funcaoID = leitor.LerInteiroUnsigned32bits("Funcao_ID");
                    
                    funcao.SetFuncaoID(funcaoID);
                    funcao.SetFuncaoDesignacao(funcaoDesignacao);
                }
            }

            else { funcao = null; }
            leitorDados.Close();
            Ligacao.Close();

            return funcao;
        }

        public bool InsertFuncao(Funcao funcao)
        {
            Type tipoFuncao = typeof(Funcao);
            
            List<string> listaColunas = LerNomesColunasTabela("Funcao");
            
            OleDbCommand comando = CriadorComandoSQL.Insert("Funcao", listaColunas, Ligacao);
 
            comando.Parameters.AddWithValue("@" + listaColunas[0], funcao.GetFuncaoDesignacao());
        
            if (Ligacao.State == ConnectionState.Closed)
            { Ligacao.Open(); }
            
            int linhasInseridas = comando.ExecuteNonQuery();
            
            Ligacao.Close();

            bool retorno = (linhasInseridas > 0) ? true : false;

            return retorno;


            }

        public bool UpdateFuncao(Funcao funcao)
        {
            Type tipoFuncao = typeof(Funcao);
            string identificador = "Funcao_ID";
            string nomeValorMudar = "Designacao";
            OleDbCommand comando = CriadorComandoSQL.UpdateSingleItemSingleValue(tipoFuncao,identificador,nomeValorMudar, Ligacao);

           
            comando.Parameters.AddWithValue("@" + nomeValorMudar, funcao.GetFuncaoDesignacao());
            comando.Parameters.AddWithValue("@" + identificador, funcao.GetFuncaoID());
           
            if (Ligacao.State == ConnectionState.Closed)
            { Ligacao.Open(); }
                       
            int linhasModificadas = comando.ExecuteNonQuery();
            
            Ligacao.Close();
            bool retorno = (linhasModificadas > 0) ? true : false;
            return retorno;
        }

        

        public bool ApagarFuncao(uint funcaoID)
        {
            bool apagouOK = false;

            Type tipoFuncao = typeof(Funcao);
            //ler da db identificador da tabela
            string nomeIdentificador = "Funcao_ID";

            OleDbCommand comandoSQL = CriadorComandoSQL.DeleteSingle(tipoFuncao, nomeIdentificador, Ligacao);

            comandoSQL.Parameters.AddWithValue("@" + nomeIdentificador, funcaoID);

            if (Ligacao.State != ConnectionState.Open) Ligacao.Open();
            
            int linhaApagada = comandoSQL.ExecuteNonQuery();
            
            if (linhaApagada > 0) apagouOK = true;

            Ligacao.Close();
            return apagouOK;
        }

        public int ApagarTodasFuncoes()
        {
            Type tipoFuncao = typeof(Funcao);
            OleDbCommand comandoSQL = CriadorComandoSQL.DeleteAll(tipoFuncao, Ligacao);
            if (Ligacao.State != ConnectionState.Open) Ligacao.Open();
            int linhasApagadas = comandoSQL.ExecuteNonQuery();
            Ligacao.Close();
            
            return linhasApagadas;
        }
    }
}


 
        
    
