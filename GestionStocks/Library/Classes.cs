﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{

    #region Colaborador
    public class Colaborador
    {
        //atributos
        private uint colaboradorMatricula;
        private string loginColaborador;
        private string nomeColaborador;
        private Funcao funcaoColaborador;

        //Constructores
        public Colaborador()
        { }

        public Colaborador(uint colaboradorMatricula, string loginColaborador, string nomeColaborador, Funcao funcaoColaborador)
        {
            this.colaboradorMatricula = colaboradorMatricula;
            this.loginColaborador = loginColaborador;
            this.nomeColaborador = nomeColaborador;
            this.funcaoColaborador = funcaoColaborador;
           
        }
        //Gets
        public uint GetColaboradorMatricula()
        {
            return this.colaboradorMatricula;
        }
        public string GetLoginColaborador()
        {
            return this.loginColaborador;
        }
        public string GetNomeColaborador()
        {
            return this.nomeColaborador;
        }
        public Funcao GetFuncaoColaborador()
        {
            return this.funcaoColaborador;
        }

        //Sets
        public void SetNomeColaborador(string nomeColaborador)
        {
            this.nomeColaborador = nomeColaborador;
        }
        public void SetFuncaoColaborador(Funcao funcaoColaborador)
        {
            this.funcaoColaborador = funcaoColaborador;
        }
        public bool SetColaboradorMatricula(uint novaMatricula)
        {
            if (MatriculaExiste(novaMatricula)) return false;
            else { this.colaboradorMatricula = novaMatricula; return true; }
        }
        
        public void SetLoginColaborador(string loginColaborador)
        {
            this.loginColaborador = loginColaborador;
        }

        //funcoes auxiliares
        private bool MatriculaExiste(uint novaMatricula)
        {
            if (BLL.ValidarNovaMatricula(novaMatricula))
            { return false; }
            else
            { return true; }
        }

       
    }
    #endregion

    #region Funcao Colaborador

    public class Funcao
    {
        //atributos
        private uint funcaoID;
        private string funcaoDesignacao;
        public static string funcaoPadrao = "CDC";

        //constructores
        public Funcao() { }

        //constructor para criar novas funcoes
        public Funcao(string funcaoDesignacao)
        {
            if (FuncaoDesignacaoExiste(funcaoDesignacao)) funcaoDesignacao=funcaoPadrao;
            else { this.funcaoDesignacao = funcaoDesignacao; }
        }

        //constructor usado para editar funcoes
        public Funcao(uint id, string designacao)
        {
            this.funcaoID = id;
            this.funcaoDesignacao = designacao;
        }
        
        //gets
        
        public uint GetFuncaoID()
        {
            return this.funcaoID;
        }
        
        public string GetFuncaoDesignacao()
        {
            return this.funcaoDesignacao;
        }

        //sets

        public void SetFuncaoID(uint funcaoID)
        {
            this.funcaoID = funcaoID;
        }

        public void  SetFuncaoDesignacao(string funcaoDesignacao)
        {
            this.funcaoDesignacao = funcaoDesignacao;
        }

        //funcoes auxiliares

        public bool FuncaoDesignacaoExiste(string funcaoDesignacao)
        {
            if (BLL.VerificarFuncaoDesignacaoExiste(funcaoDesignacao))
            {
                return true; 
            }
            else
            {
                return false;
            }
        }

        public Funcao LerFuncaoPorID(uint funcaoID)
        {
            DAL dal = new DAL();
            Funcao funcao = dal.SelectFuncaoSingle(funcaoID);
            return funcao;
        }

        public static bool ApagarFuncao(uint funcaoID)
        {
            DAL dal = new DAL();
            bool apagouOK = dal.ApagarFuncao(funcaoID);
            return apagouOK;
        }

        //public bool FuncaoIDExiste(uint funcaoID)
        //{
        //    if (BLL.VerificarFuncaoIDExiste(funcaoID))
        //    { return false; }
        //    else
        //    { return true; }
        //}



    }



    #endregion


    #region Utilizador
    public class Utilizador
    {
        private uint utilizadorID;
        private string utilizadorNome;
        private string utilizadorLogin;
        private string utilizadorPassword;


        public Utilizador()
        {
            this.utilizadorID = 0;
            this.utilizadorNome = "0";
            this.utilizadorLogin = "0";
            this.utilizadorPassword = "0";

        }

        //constructor usado para ler da base de dados (com todos os campos, inlcuindo ID da BD)
        public Utilizador(uint utilizadorID, string utilizadorNome, string utilizadorLogin, string utilizadorPassword)
        {
            this.utilizadorID = utilizadorID;
            this.utilizadorNome = utilizadorNome;
            this.utilizadorLogin = utilizadorLogin;
            this.utilizadorPassword = utilizadorPassword;

        }

        //constructor sem ID para criar Utilizadores novos
        public Utilizador(string utilizadorNome, string utilizadorLogin, string utilizadorPassword)
        {

            this.utilizadorNome = utilizadorNome;
            this.utilizadorLogin = utilizadorLogin;
            this.utilizadorPassword = utilizadorPassword;

        }


        //constructor ?
        public Utilizador(uint utilizadorID, string utilizadorNome, string utilizadorLogin)
        {
            this.utilizadorID = utilizadorID;
            this.utilizadorNome = utilizadorNome;
            this.utilizadorLogin = utilizadorLogin;
        }


        public uint GetUtilizadorID()
        {
            return this.utilizadorID;
        }

        public string GetUtilizadorNome()
        {
            return this.utilizadorNome;
        }

        public void SetUtilizadorNome(string utilizadorNome)
        {
            this.utilizadorNome = utilizadorNome;
        }

        public string GetUtilizadorLogin()
        {
            return this.utilizadorLogin;
        }

        public void SetUtilizadorLogin(string utilizadorLogin)
        {
            this.utilizadorLogin = utilizadorLogin;
        }

        public string GetUtilizadorPassword()
        {
            return this.utilizadorPassword;
        }

        public void SetUtilizadorPassword(string funcaoColaborador)
        {
            this.utilizadorPassword = funcaoColaborador;
        }

    }
    #endregion
}
